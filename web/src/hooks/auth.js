import React, { createContext, useState, useCallback, useContext } from 'react';
import PropTypes from 'prop-types';

import api from '../services/api';

const AuthContext = createContext({});

const AuthProvider = ({ children }) => {
    const [data, setData] = useState(() => {
        const uid = sessionStorage.getItem('@empresa-web:uid');
        const user = sessionStorage.getItem('@empresa-web:user');
        const client = sessionStorage.getItem('@empresa-web:client');
        const access_token = sessionStorage.getItem('@empresa-web:access_token');

        if (uid && access_token && client) {
            api.defaults.headers.common['uid'] = uid;
            api.defaults.headers.common['client'] = client;
            api.defaults.headers.common['access-token'] = access_token;

            return { investor: JSON.parse(user) };
        }
        return {};
    });

    const signIn = useCallback(async ({ email, password }) => {
        const response = await api.post('api/v1/users/auth/sign_in', { email, password });

        const { investor } = response.data;
        const { uid, client } = response.headers;
        const access_token = response.headers['access-token'];

        sessionStorage.setItem('@empresa-web:user', JSON.stringify(investor));
        sessionStorage.setItem('@empresa-web:uid', uid);
        sessionStorage.setItem('@empresa-web:client', client);
        sessionStorage.setItem('@empresa-web:access_token', access_token);

        api.defaults.headers.common['uid'] = uid;
        api.defaults.headers.common['client'] = client;
        api.defaults.headers.common['access-token'] = access_token;

        setData({ investor });
    });

    const signOut = useCallback(() => {
        sessionStorage.removeItem('@empresa-web:user');
        sessionStorage.removeItem('@empresa-web:uid');
        sessionStorage.removeItem('@empresa-web:client');
        sessionStorage.removeItem('@empresa-web:access_token');

        setData({});
    }, []);

    return (
        <AuthContext.Provider value={{ user: data.investor, signIn, signOut }}>
            {children}
        </AuthContext.Provider>
    );
};

AuthProvider.propTypes = {
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired
};

const useAuth = () => {
    const context = useContext(AuthContext);
    return context;
};

export { AuthContext, AuthProvider, useAuth };
