import styled from 'styled-components';

export const Container = styled.div`
    width: 100%;
    padding: 16px 0px 0px 0;
    margin: 30px 0;
    border-bottom: 1px solid #fff;
    color: #ee4c77;

    display: flex;
    align-items: center;

    & + div {
        margin-top: 8px;
    }

    input {
        flex: 1;
        background: transparent;
        border: 0;
        color: #fff;
        font-size: 16px;
        font-weight: 500;

        &:focus {
            outline: none;
        }

        &::placeholder {
            color: #666360;
        }
    }

    svg {
        margin-right: 16px;
    }

    svg:hover {
        cursor: pointer;
    }
`;
