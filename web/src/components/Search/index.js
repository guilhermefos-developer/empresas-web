import React, { useRef, useCallback } from 'react';
import { FiSearch, FiX } from 'react-icons/fi';

import PropTypes from 'prop-types';

import { Container } from './style';

const Search = ({ onClose, onChange, ...rest }) => {
    const inputRef = useRef(null);

    const handleCloseSearch = useCallback(() => {
        onClose();
    }, []);

    const handleOnChange = useCallback((el) => {
        onChange(el.target.value);
    }, []);

    return (
        <Container>
            <FiSearch size={20} color="#fff" />
            <input ref={inputRef} {...rest} type="text" onChange={handleOnChange} />
            <FiX onClick={handleCloseSearch} size={20} color="#fff" />
        </Container>
    );
};

Search.propTypes = {
    onClose: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired
};

export default Search;
