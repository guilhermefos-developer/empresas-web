import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    height: 110px;
    padding: 10px 100px;

    @media only screen and (max-width: 768px) {
        padding: 10px 16px;
    }

    background-image: linear-gradient(180deg, #ee4c77 35%, #c32b90 100%);

    img {
        margin: 0 auto;
        max-width: 235px;
        object-fit: contain;

        @media only screen and (max-width: 768px) {
            max-width: 180px;
        }
    }
`;

export const IconWrapper = styled.div`
    align-self: center;
    margin-right: 20px;

    &:hover {
        cursor: pointer;
    }
`;
