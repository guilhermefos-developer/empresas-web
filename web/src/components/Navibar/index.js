import React, { useState, useCallback, useEffect } from 'react';

import PropTypes from 'prop-types';

import logo from '../../assets/logo-nav.png';

import { FiSearch, FiLogOut } from 'react-icons/fi';

import { useAuth } from '../../hooks/auth';

import Search from '../../components/Search';

import { Container, IconWrapper } from './style';

const Navibar = ({ onSearching, onChange }) => {
    const { signOut } = useAuth();
    const [isSearching, setIsSearching] = useState(false);

    useEffect(() => {
        onSearching(isSearching);
    }, [isSearching]);

    const handleIsSearching = useCallback(() => {
        setIsSearching((prev) => !prev);
    }, []);

    const handleOnChange = useCallback((text) => {
        onChange(text);
    }, []);

    const handleSignOut = useCallback(() => {
        signOut();
    }, []);

    return (
        <Container>
            {!isSearching && (
                <>
                    <IconWrapper>
                        <FiLogOut onClick={handleSignOut} size={25} color="#fff" />
                    </IconWrapper>
                    <img src={logo} alt="Logo" />
                    <IconWrapper>
                        <FiSearch onClick={handleIsSearching} size={25} color="#fff" />
                    </IconWrapper>
                </>
            )}
            {isSearching && (
                <Search
                    placeholder="Pesquisar"
                    onClose={handleIsSearching}
                    onChange={handleOnChange}
                />
            )}
        </Container>
    );
};

Navibar.propTypes = {
    onSearching: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired
};

export default Navibar;
