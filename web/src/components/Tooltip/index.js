import React from 'react';
import PropTypes from 'prop-types';

import { Container } from './style';

const Tooltip = ({ title, className, children }) => (
    <Container className={className}>
        {children}
        <span>{title}</span>
    </Container>
);

Tooltip.propTypes = {
    title: PropTypes.string.isRequired,
    className: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
};

export default Tooltip;
