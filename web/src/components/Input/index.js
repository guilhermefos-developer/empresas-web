import React, { useState, useRef, useCallback, useEffect } from 'react';
import { FiAlertCircle, FiEye, FiEyeOff } from 'react-icons/fi';
import { useField } from '@unform/core';

import PropTypes from 'prop-types';

import { Container, Error } from './styles';

const Input = ({ name, type, icon: Icon, ...rest }) => {
    const inputRef = useRef(null);

    const [showPassword, setShowPassword] = useState(false);

    const [isFocused, setIsFocused] = useState(false);
    const [isFilled, setIsFilled] = useState(false);

    const { fieldName, defaultValue, error, registerField } = useField(name);

    const handleInputFocus = useCallback(() => {
        setIsFocused(true);
    }, []);

    const handleInputBlur = useCallback(() => {
        setIsFocused(false);
        setIsFilled(!!inputRef.current?.value);
    }, []);

    useEffect(() => {
        registerField({
            name: fieldName,
            ref: inputRef.current,
            path: 'value'
        });
    }, [fieldName, registerField]);

    return (
        <Container isErrored={!!error} isFilled={isFilled} isFocused={isFocused}>
            {Icon && <Icon size={20} />}
            <input
                onFocus={handleInputFocus}
                onBlur={handleInputBlur}
                defaultValue={defaultValue}
                ref={inputRef}
                {...rest}
                type={!type ? 'text' : type === 'password' && showPassword ? 'text' : type}
            />
            {type === 'password' && showPassword && (
                <FiEye size={20} color="#383743" onClick={() => setShowPassword(false)} />
            )}
            {type === 'password' && !showPassword && (
                <FiEyeOff size={20} color="#383743" onClick={() => setShowPassword(true)} />
            )}
            {error && (
                <Error title={error}>
                    <FiAlertCircle color="#c53030" size={20} />
                </Error>
            )}
        </Container>
    );
};

Input.propTypes = {
    name: PropTypes.string.isRequired,
    type: PropTypes.string,
    icon: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired
};

export default Input;
