import styled, { css } from 'styled-components';

import Tooltip from '../Tooltip';

export const Container = styled.div`
    width: 100%;
    padding: 16px 0px 8px 0;
    border-bottom: 0.6px solid #666360;
    color: #ee4c77;

    display: flex;
    align-items: center;

    & + div {
        margin-top: 8px;
    }

    ${({ isErrored }) =>
        isErrored &&
        css`
            border-color: #c53030;
        `}

    ${({ isFocused }) =>
        isFocused &&
        css`
            color: #ee4c77;
            border-color: #ee4c77;
        `}

  ${({ isFilled }) =>
        isFilled &&
        css`
            color: #ee4c77;
        `}

  input {
        flex: 1;
        background: transparent;
        border: 0;
        color: #383743;

        &:focus {
            outline: none;
        }

        &::placeholder {
            color: #666360;
        }
    }

    svg {
        margin-right: 16px;
    }
`;

export const Error = styled(Tooltip)`
    height: 20px;
    margin-left: 16px;

    svg {
        margin: 0;
    }

    span {
        background: #c53030;
        color: #fff;

        &::before {
            border-color: #c53030 transparent;
        }
    }
`;
