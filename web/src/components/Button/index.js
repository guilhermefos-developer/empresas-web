import React from 'react';

import PropTypes from 'prop-types';

import { Container } from './style';

const Button = ({ children, disabled }) => {
    return <Container disabled={disabled}>{children}</Container>;
};

Button.defaultProps = {
    disabled: false
};

Button.propTypes = {
    disabled: PropTypes.bool,
    children: PropTypes.string.isRequired
};

export default Button;
