import styled from 'styled-components';
import { shade } from 'polished';

export const Container = styled.button`
    width: 300px;
    height: 57px;
    margin: 30px 0 0;
    padding: 18px 134px 18px 134px;
    border: none;
    border-radius: 3.9px;
    background-color: ${({ disabled }) => (disabled ? '#748383' : '#57bbbc')};

    color: #fff;
    font-family: GillSans;
    font-size: 19.3px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: center;

    &:hover {
        background-color: ${({ disabled }) => (disabled ? '#748383' : shade(0.2, '#57bbbc'))};
    }
`;
