/* eslint-disable jest/expect-expect */
import React from 'react';
import { render, fireEvent, waitFor, act } from '@testing-library/react';
import SignIn from '../../pages/SignIn';

jest.mock('../../hooks/auth', () => {
    return {
        useAuth: () => ({
            signIn: jest.fn()
        })
    };
});

describe('SignIn Page', () => {
    it('should be able to sign in', async () => {
        const { getByPlaceholderText, getByText } = render(<SignIn />);

        const emailField = getByPlaceholderText('E-mail');
        const passwordField = getByPlaceholderText('Senha');
        const buttonElement = getByText('Entrar');

        fireEvent.change(emailField, { target: { value: 'testeapple@ioasys.com.br' } });
        fireEvent.change(passwordField, { target: { value: '12341234' } });
        fireEvent.click(buttonElement);
    });

    it('should not be able to sign in wihout filling the inputs', async () => {
        const { getByText } = render(<SignIn />);

        const buttonElement = getByText('Entrar');

        fireEvent.click(buttonElement);

        await waitFor(() => {
            expect(getByText('Email obrigatório')).toBeTruthy();
            expect(getByText('Senha obrigatória')).toBeTruthy();
        });
    });

    it('should not be able to sign in an unauthorized user', async () => {
        const { getByPlaceholderText, getByText, asFragment } = render(<SignIn />);
        const firstRender = asFragment();

        const emailField = getByPlaceholderText('E-mail');
        const passwordField = getByPlaceholderText('Senha');
        const buttonElement = getByText('Entrar');

        fireEvent.change(emailField, { target: { value: 'testpple@ioasys.com.br' } });
        fireEvent.change(passwordField, { target: { value: '12341234' } });
        fireEvent.click(buttonElement);

        expect(firstRender).toMatchInlineSnapshot(asFragment(), `Object {}`);
    });
});
