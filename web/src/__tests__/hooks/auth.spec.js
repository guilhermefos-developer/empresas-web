import { renderHook, act } from '@testing-library/react-hooks';
import { useAuth, AuthProvider } from '../../hooks/auth';
import MockAdapter from 'axios-mock-adapter';
import api from '../../services/api';

const apiMock = new MockAdapter(api);

describe('Auth hook', () => {
    it('should be able to sign in', async () => {
        apiMock.onPost('api/v1/users/auth/sign_in').reply(
            200,
            {
                investor: {
                    id: 1,
                    email: 'testeapple@ioasys.com.br'
                }
            },
            {
                'access-token': 'tkOKQFuOGdalnxuydE-Ysw',
                uid: 'testeapple@ioasys.com.br',
                client: 'noGl29vPQcaxxtsjsNyxpg'
            }
        );

        const setItemSpy = jest.spyOn(Storage.prototype, 'setItem');

        const { result, waitForNextUpdate } = renderHook(() => useAuth(), {
            wrapper: AuthProvider
        });

        result.current.signIn({
            email: 'testeapple@ioasys.com.br',
            password: '12341234'
        });

        await waitForNextUpdate();

        expect(setItemSpy).toHaveBeenCalledTimes(4);
        expect(result.current.user.email).toEqual('testeapple@ioasys.com.br');
    });

    it('should not be able to sigin an unauthorized user', async () => {
        apiMock.onPost('api/v1/users/auth/sign_in').reply(401, {
            success: false,
            errors: ['Invalid login credentials. Please try again.']
        });

        const { result } = renderHook(() => useAuth(), {
            wrapper: AuthProvider
        });

        await expect(
            result.current.signIn({
                email: 'testeapple@ioasys.com.br',
                password: '12341234'
            })
        ).rejects.toBeInstanceOf(Error);
    });

    it('should be able to restore saved data from storage', () => {
        const getItemSpy = jest.spyOn(Storage.prototype, 'getItem').mockImplementation((key) => {
            switch (key) {
                case '@empresa-web:uid':
                    return 'testeapple@ioasys.com.br';
                case '@empresa-web:user':
                    return JSON.stringify({
                        id: 1,
                        email: 'testeapple@ioasys.com.br'
                    });
                case '@empresa-web:client':
                    return 'noGl29vPQcaxxtsjsNyxpg';
                case '@empresa-web:access_token':
                    return 'tkOKQFuOGdalnxuydE-Ysw';

                default:
                    return undefined;
            }
        });

        const { result } = renderHook(() => useAuth(), {
            wrapper: AuthProvider
        });

        expect(getItemSpy).toHaveBeenCalledTimes(4);
        expect(result.current.user.email).toEqual('testeapple@ioasys.com.br');
    });

    it('should be able to signout a user', () => {
        const removeItemSpy = jest.spyOn(Storage.prototype, 'removeItem');

        const { result } = renderHook(() => useAuth(), {
            wrapper: AuthProvider
        });

        act(() => {
            result.current.signOut();
        });

        expect(removeItemSpy).toHaveBeenCalledTimes(4);
        expect(result.current.user).toBeUndefined();
    });
});
