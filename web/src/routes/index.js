import React from 'react';
import { Switch } from 'react-router-dom';

import Route from './Route';

import SignIn from '../pages/SignIn';
import Enterprises from '../pages/Enterprises';
import EnterpriseDetail from '../pages/EnterpriseDetail';

const Routes = () => (
    <Switch>
        <Route path="/" component={SignIn} exact />
        <Route path="/enterprises" component={Enterprises} isPrivate />
        <Route path="/enterprise/detail/:id" component={EnterpriseDetail} isPrivate />
    </Switch>
);

export default Routes;
