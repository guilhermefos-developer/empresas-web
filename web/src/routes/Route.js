import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route as ReactDOMRoute } from 'react-router-dom';

import { useAuth } from '../hooks/auth';

const Route = ({ isPrivate, component: Component, ...rest }) => {
    const { user } = useAuth();

    return (
        <ReactDOMRoute
            {...rest}
            render={({ location }) => {
                return isPrivate === !!user ? (
                    <Component />
                ) : (
                    <Redirect
                        to={{
                            pathname: !isPrivate ? '/enterprises' : '/',
                            state: { from: location }
                        }}
                    />
                );
            }}
        />
    );
};

Route.defaultProps = {
    isPrivate: false
};

Route.propTypes = {
    isPrivate: PropTypes.bool,
    component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired
};

export default Route;
