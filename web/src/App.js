import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import Route from './routes';

import AppProvider from './hooks';

const App = () => (
    <Router>
        <AppProvider>
            <Route />
        </AppProvider>
    </Router>
);

export default App;
