import React, { useRef, useCallback, useState } from 'react';
import { Ring } from 'react-spinners-css';
import { Form } from '@unform/web';
import * as Yup from 'yup';
import { FiMail, FiLock } from 'react-icons/fi';

import { useAuth } from '../../hooks/auth';

import getValidationErrors from '../../utils/getValidationErrors';

import Button from '../../components/Button';
import Input from '../../components/Input';

import logo from '../../assets/logo-home.png';

import {
    Container,
    Content,
    Welcome,
    Description,
    ButtonWrapper,
    Unauthorized,
    LoadingWrapper
} from './style';

const SignIn = () => {
    const formRef = useRef(null);
    const { signIn } = useAuth();

    const [isLoading, setIsLoading] = useState(false);
    const [unauthorized, setUnauthorized] = useState(false);
    const [unauthorizedMessage, setUnauthorizedMessage] = useState('');

    const handleSubmit = useCallback(async (data) => {
        try {
            setIsLoading(true);
            formRef.current?.setErrors({});

            const schema = Yup.object().shape({
                email: Yup.string().required('Email obrigatório'),
                password: Yup.string().required('Senha obrigatória')
            });

            await schema.validate(data, {
                abortEarly: false
            });

            await signIn(data);
        } catch (error) {
            setIsLoading(false);

            if (error instanceof Yup.ValidationError) {
                const errors = getValidationErrors(error);
                formRef.current?.setErrors(errors);
                return;
            }

            if (error instanceof Object) {
                const { response } = error;
                if (response.status === 401) {
                    setUnauthorized(true);
                    setUnauthorizedMessage(response.data.errors[0]);
                }
            }
        }
    }, []);

    const handleOnChange = useCallback(() => {
        if (unauthorized) setUnauthorized(false);
    }, [unauthorized]);

    return (
        <Container>
            {isLoading && (
                <LoadingWrapper>
                    <Ring color="#57bbbc" />
                </LoadingWrapper>
            )}
            <Content>
                <img src={logo} alt="Logo" />
                <Welcome>BEM-VINDO AO EMPRESAS</Welcome>
                <Description>
                    Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
                </Description>

                <Form ref={formRef} onSubmit={handleSubmit}>
                    <Input
                        icon={FiMail}
                        name="email"
                        type="email"
                        placeholder="E-mail"
                        onChange={handleOnChange}
                    />

                    <Input
                        icon={FiLock}
                        name="password"
                        type="password"
                        placeholder="Senha"
                        onChange={handleOnChange}
                    />

                    {unauthorized && <Unauthorized>{unauthorizedMessage}</Unauthorized>}

                    <ButtonWrapper>
                        <Button disabled={unauthorized}>Entrar</Button>
                    </ButtonWrapper>
                </Form>
            </Content>
        </Container>
    );
};

export default SignIn;
