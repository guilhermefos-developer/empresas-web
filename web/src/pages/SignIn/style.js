import styled from 'styled-components';

export const Container = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background: #ebe9d7;
`;

export const LoadingWrapper = styled.div`
    z-index: 1000;
    position: absolute;
`;

export const Content = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 300px;
    padding: 10px;

    img {
        width: 295px;
        margin: 0 auto;
        object-fit: contain;
    }
`;

export const Welcome = styled.h1`
    font-size: 25.6px;
    font-weight: bold;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: -1.2px;
    text-align: center;
    color: #383743;
`;

export const Description = styled.p`
    font-size: 17px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.48;
    letter-spacing: 0.2px;
    text-align: center;
    color: #383743;
`;

export const ButtonWrapper = styled.div`
    margin: 0 auto;
`;

export const Unauthorized = styled.p`
    font-size: 12px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.48;
    letter-spacing: 0.2px;
    text-align: center;
    color: red;
`;
