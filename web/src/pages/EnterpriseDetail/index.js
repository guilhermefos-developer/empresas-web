import React, { useCallback, useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi';
import { Ring } from 'react-spinners-css';

import api from '../../services/api';

import {
    Navbar,
    Title,
    Container,
    LoadingWrapper,
    EnterpriseWrapper,
    EnterpriseCover,
    EnterpriseDescription
} from './style';

const EnterpriseDetail = () => {
    const { id } = useParams();
    const history = useHistory();

    const [enterprise, setEnterprise] = useState({});
    const [isLoading, setIsLoading] = useState(true);

    const handleBack = useCallback(() => {
        history.goBack();
    }, []);

    useEffect(() => {
        async function fetchEnterprise() {
            const response = await api.get(`api/v1/enterprises/${id}`);
            const { enterprise } = response.data;
            setEnterprise(enterprise);
            setIsLoading(false);
        }

        fetchEnterprise();
    }, []);

    return (
        <>
            {isLoading ? (
                <LoadingWrapper>
                    <Ring color="#57bbbc" />
                </LoadingWrapper>
            ) : (
                <>
                    <Navbar>
                        <FiArrowLeft size={30} color="#fff" onClick={handleBack} />
                        <Title>{enterprise.enterprise_name}</Title>
                    </Navbar>
                    <Container>
                        <EnterpriseWrapper>
                            <EnterpriseCover>{enterprise.enterprise_name}</EnterpriseCover>
                            <EnterpriseDescription>{enterprise.description}</EnterpriseDescription>
                        </EnterpriseWrapper>
                    </Container>
                </>
            )}
        </>
    );
};

export default EnterpriseDetail;
