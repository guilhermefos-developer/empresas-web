import styled from 'styled-components';

export const Container = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    background: #ebe9d7;
`;

export const LoadingWrapper = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    background: #ebe9d7;
`;

export const Navbar = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    height: 110px;
    padding: 10px 100px;

    @media only screen and (max-width: 768px) {
        padding: 10px 16px;
    }

    background-image: linear-gradient(180deg, #ee4c77 35%, #c32b90 100%);
`;

export const Title = styled.span`
    font-size: 34px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #fff;
    margin-left: 20px;
`;

export const EnterpriseWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 85%;
    height: 400px;
    margin: 10px auto;
    background: #fff;
    border-radius: 5px;
    padding: 20px;
`;

export const EnterpriseCover = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 200px;
    background: #7dc075;
`;

export const EnterpriseDescription = styled.p`
    font-size: 20px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #8d8c8c;
`;
