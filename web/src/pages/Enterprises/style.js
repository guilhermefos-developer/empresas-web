import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    background: #ebe9d7;
    padding: 16px;
    min-height: 100vh;

    h1 {
        margin-top: 200px;
        font-size: 28px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.22;
        letter-spacing: -0.45px;
        text-align: center;
    }

    h2 {
        margin-top: 200px;
        font-size: 28px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.22;
        letter-spacing: -0.45px;
        text-align: center;
        color: #b5b4b4;
    }
`;

export const EnterpriseWrapper = styled.div`
    width: 85%;
    margin: 0 auto;
    margin-bottom: 15px;

    /* margin-top: ${({ isSearch }) => (isSearch ? '-550px' : '')}; */

    a {
        text-decoration: none;
        display: flex;
        background: #fff;
        border-radius: 5px;
        padding: 20px;

        transition: transform 0.2s;

        &:hover {
            transform: translateX(10px);
        }
    }
`;

export const EnterpriseThumbrl = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    background: #7dc075;
    height: 120px;
    width: 290px;
    margin-right: 20px;
    color: #fff;
    font-size: 42px;
    font-weight: bold;
`;

export const EnterpriseContent = styled.div`
    display: flex;
    flex-direction: column;
`;

export const EnterpriseCompany = styled.span`
    color: #0d0430;
    font-size: 18px;
    font-weight: bold;
    margin: 10px 0 0 0;
`;

export const EnterpriseBusiness = styled.span`
    color: #8d8c8c;
    font-size: 14px;
    font-weight: normal;
    margin: 5px 0 0 0;
`;

export const EnterpriseCountry = styled.span`
    color: #8d8c8c;
    font-size: 12px;
    font-weight: normal;
    margin: 5px 0 0 0;
`;

export const LoadingWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    min-height: 85vh;
`;
