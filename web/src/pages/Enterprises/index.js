import React, { useEffect, useState, useCallback } from 'react';
import { Link } from 'react-router-dom';
import { Ring } from 'react-spinners-css';

import api from '../../services/api';

import Navbar from '../../components/Navibar';

import {
    Container,
    LoadingWrapper,
    EnterpriseWrapper,
    EnterpriseCompany,
    EnterpriseContent,
    EnterpriseThumbrl,
    EnterpriseBusiness,
    EnterpriseCountry
} from './style';

const Enterprises = () => {
    const [enterprises, setEnterprises] = useState([]);
    const [foundEnterprise, setFoundEnterprise] = useState(undefined);
    const [isLoading, setIsLoading] = useState(true);
    const [isSearching, setIsSearching] = useState(false);
    const [searchText, setSearchText] = useState('');

    useEffect(() => {
        async function fetchEnterprises() {
            const response = await api.get('api/v1/enterprises');
            const { enterprises } = response.data;
            setEnterprises(enterprises);
            setIsLoading(false);
        }

        fetchEnterprises();
    }, []);

    useEffect(() => {
        const timeout = setTimeout(() => enterpriseFilter(), 500);
        return () => clearTimeout(timeout);
    }, [searchText]);

    const enterpriseFilter = useCallback(async () => {
        if (searchText.length > 0) {
            setIsLoading(true);
            const response = await api.get(`api/v1/enterprises?name=${searchText}`);
            const { enterprises } = response.data;
            setIsLoading(false);
            setFoundEnterprise(enterprises);
        }
    }, [searchText]);

    const handleOnSearching = useCallback((isSearching) => {
        if (!isSearching) setFoundEnterprise(undefined);
        setIsSearching(isSearching);
    }, []);

    const handleOnChange = useCallback(
        (text) => {
            setSearchText(text);
        },
        [searchText]
    );

    return (
        <>
            <Navbar onSearching={handleOnSearching} onChange={handleOnChange} />
            <Container>
                {isLoading ? (
                    <LoadingWrapper>
                        <Ring color="#57bbbc" />
                    </LoadingWrapper>
                ) : (
                    <>
                        {!isSearching && (
                            <>
                                {enterprises.map((enterprise, i) => (
                                    <EnterpriseWrapper key={enterprise.id}>
                                        <Link to={`enterprise/detail/${enterprise.id}`}>
                                            <EnterpriseThumbrl>E{i + 1}</EnterpriseThumbrl>
                                            <EnterpriseContent>
                                                <EnterpriseCompany>
                                                    {enterprise.enterprise_name}
                                                </EnterpriseCompany>
                                                <EnterpriseBusiness>
                                                    {
                                                        enterprise.enterprise_type
                                                            .enterprise_type_name
                                                    }
                                                </EnterpriseBusiness>
                                                <EnterpriseCountry>
                                                    {enterprise.country}
                                                </EnterpriseCountry>
                                            </EnterpriseContent>
                                        </Link>
                                    </EnterpriseWrapper>
                                ))}
                            </>
                        )}
                        {isSearching && !foundEnterprise && <h1>Clique na busca para iniciar</h1>}
                        {isSearching && foundEnterprise?.length === 0 && (
                            <h2>Nenhuma empresa foi encontrada para a busca realizada.</h2>
                        )}
                        {isSearching &&
                            foundEnterprise?.length > 0 &&
                            foundEnterprise.map((enterprise, i) => (
                                <EnterpriseWrapper isSearch key={enterprise.id}>
                                    <Link to={`enterprise/detail/${enterprise.id}`}>
                                        <EnterpriseThumbrl>E{i + 1}</EnterpriseThumbrl>
                                        <EnterpriseContent>
                                            <EnterpriseCompany>
                                                {enterprise.enterprise_name}
                                            </EnterpriseCompany>
                                            <EnterpriseBusiness>
                                                {enterprise.enterprise_type.enterprise_type_name}
                                            </EnterpriseBusiness>
                                            <EnterpriseCountry>
                                                {enterprise.country}
                                            </EnterpriseCountry>
                                        </EnterpriseContent>
                                    </Link>
                                </EnterpriseWrapper>
                            ))}
                    </>
                )}
            </Container>
        </>
    );
};

export default Enterprises;
